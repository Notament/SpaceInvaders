function moveUfo1(){
  $(".ufo").animate({
    "left" : "10%",
  },{
    duration : 1000,
    queue : false,
  });
  $(".ufo").animate({
    "top" : "40%",
  },{
    duration : 1000,
    queue : false,
  })
setTimeout(function(){moveUfo2();},1000)
}
function moveUfo2(){
  $(".ufo").animate({
    "left" : "20%",
  },{
    duration : 1000,
    queue : false,
  });
  $(".ufo").animate({
    "top" : "50%",
  },{
    duration : 1000,
    queue : false,
  })
  setTimeout(function(){moveUfo3();},1000)
}
function moveUfo3(){
  $(".ufo").animate({
    "left" : "30%",
  },{
    duration : 1000,
    queue : false,
  });
  $(".ufo").animate({
    "top" : "40%",
  },{
    duration : 1000,
    queue : false,
  })
  setTimeout(function(){moveUfo4();},1000)
}
function moveUfo4(){
  $(".ufo").animate({
    "left" : "40%",
  },{
    duration : 1000,
    queue : false,
  });
  $(".ufo").animate({
    "top" : "50%",
  },{
    duration : 1000,
    queue : false,
  })
  setTimeout(function(){moveUfo5();},1000)
}
function moveUfo5(){
  $(".ufo").animate({
    "left" : "50%",
  },{
    duration : 1000,
    queue : false,
  });
  $(".ufo").animate({
    "top" : "40%",
  },{
    duration : 1000,
    queue : false,
  })
  setTimeout(function(){moveUfo6();},1000)
}
function moveUfo6(){
  $(".ufo").animate({
    "left" : "60%",
  },{
    duration : 1000,
    queue : false,
  });
  $(".ufo").animate({
    "top" : "50%",
  },{
    duration : 1000,
    queue : false,
  })
  setTimeout(function(){moveUfo7();},1000)
}
function moveUfo7(){
  $(".ufo").animate({
    "left" : "70%",
  },{
    duration : 1000,
    queue : false,
  });
  $(".ufo").animate({
    "top" : "40%",
  },{
    duration : 1000,
    queue : false,
  })
  setTimeout(function(){moveUfo8();},1000)
}
function moveUfo8(){
  $(".ufo").animate({
    "left" : "80%",
  },{
    duration : 1000,
    queue : false,
  });
  $(".ufo").animate({
    "top" : "50%",
  },{
    duration : 1000,
    queue : false,
  })
  setTimeout(function(){moveUfo9();},1000)
}
function moveUfo9(){
  $(".ufo").animate({
    "left" : "90%",
  },{
    duration : 1000,
    queue : false,
  });
  $(".ufo").animate({
    "top" : "40%",
  },{
    duration : 1000,
    queue : false,
  })
  setTimeout(function(){moveUfo1();},1000)
}
moveUfo1();


$(".ufo").click(function(){

  $(".displayDebut").css({
    display : "none",
  })
  enemyShot();
});

function enemyShot(){
  if(remaining > 0 && lives > 0){
  var enemyReload = 1000;
  var preventReload = enemyReload/2;
  var posleftES = 0;
  posleftES = Math.round(Math.random() * 1000);
  if(posleftES < 177.5){
    posleftES = posleftES + 177.5;
  }
  setTimeout(function(){
    $(".gameContainer").append('<div class="futurPS">!</div>');
    $(".futurPS:last").addClass("prevent");
    $(".prevent:last").css({
      left : posleftES,
      top : 104,
    });
  },preventReload)

  setTimeout(function(){
    $(".prevent:first").remove();
    $(".enemyShoot.first").remove();
    $(".gameContainer").append('<div class="futurES"></div>');
    $(".futurES:last").addClass("enemyShoot");
    $(".enemyShoot:last").css({
      left : posleftES,
      top : 102,
    });
    $(".enemyShoot:last").animate({
      "height" : "500px",
    },{
      duration : 1000,
      step : function(){
        if(checkColision3()){
          tankDelete();
          livesLeft();
        }
      }
    });
    $(".enemyShoot:last").animate({
      "height" : "0px",
    },{
      duration : 1000,
      complete : function(){
      $(".futurES:first").remove();
      $(".futurPS:first").remove();
    }
    });
    enemyShot();
  },enemyReload);
}}
var lives = 5;

function livesLeft(){
  lives--;
  switch(lives){
    case 4:
    $(".vieJS").css({
      'background-image' : 'url("assets/heart1.png")'
    });
    break;
    case 3:
    $(".vieJS").css({
      'background-image' : 'url("assets/heart2.png")'
    });
    break;
    case 2:
    $(".vieJS").css({
      'background-image' : 'url("assets/heart3.png")'
    });
    break;
    case 1:
    $(".vieJS").css({
      'background-image' : 'url("assets/heart4.png")'
    });
    break;
    case 0:
    $(".vieJS").css({
      'background-image' : 'url("assets/heart5.png")'
    });
    respawn = 3;
    checkAlive();
    break;
  }
}
var respawn = 0;
function tankDelete(){
  $(".tank").css({
    display : 'none',
  })
  $(".gameContainer").append('<div class="futurEx"></div>');
  $(".futurEx:last").addClass("explo");
  $(".explo").css({
    "left" : a,
    "top" : tanktop,
  })
  respawn ++;
  checkAlive();
};

function checkAlive(){
  if( respawn === 1){
    $(".respawn").css({
      "display" : "block"
    })
    score = score - 50;
    $(".scorejs").html(score);
  }
  else if (respawn === 0){
    $(".tank").css({
      "display": "flex"
    });
    $(".respawn").css({
      "display" : "none"
    });
    $(".explo:last").remove();
  }
  else if (respawn === 3){
    $(".loose").css({
      "display": "flex",
      "flex-direction" : "column",
      "justify-content" : "space-around",
    });
    $(".loose").draggable({
      containment : "parent"
    });
    $(".respawn").css({
      "display" : "none",
    })
  }
};

$(".respawn").click(function(){

  if(respawn < 3){
  respawn--;
  checkAlive();
}
})

var tank = $(".tank");
var tankpos = tank.position();
var a = tankpos.left;

$(document).keydown(function(appui){
 if(appui.keyCode == 39 && a < 1050){
   a = a + 5;
   $(".tank").css({
     left : a,
   });
 }
 else if (appui.keyCode == 37 && a > 150){
    a = a - 5;
    $(".tank").css({
      left : a,
    });
 }
})

var stop = false;
$(document).keyup(function(appui){
  if(appui.keyCode == 32 && stop == false) {
    stop = true;
    shot();
    setTimeout(function () {
      stop = false;
    }, 1800);
  }
});

function shot () {
  if(remaining > 0){
  reload();
  $(".gameContainer").append('<div class="futurS"></div>');
  $(".futurS:last").addClass("allyShoot");
  $(".allyShoot:last").css({
    "left" : a + 28,
    "top"  : tankpos.top - 15,
  });
  $(".allyShoot:last").animate({
    top: "-=500px",
  },{
    duration : 2000,
    step : function(){{if(checkColision()){
      deletAlien();
      }else if(checkColision2()){
      deletVoid();
      }}},
  });
}}

function reload(){
  $(".cool").css({
    "width" : "0px",
  })
  $(".cool").animate({
    width: "100%",
  },1800);
}


var score = 0;
$(".scorejs").html(score);
function scoresShow(){
  score = score + 10;
  $(".scorejs").html(score);
};
var remaining = 55;

function deletAlien(){
  elem = document.elementFromPoint(alposl, alpost);
  elem.remove();
  $(".allyShoot").css({
    display : "none",
  })
  $(".gameContainer").append('<div class="futurD"></div>');
  $(".futurD:last").addClass("dieEffect");
  $(".dieEffect").css({
    "top" : alpost ,
    "left" : alposl ,
  });
  scoresShow();
  remaining--;
  win();
  setTimeout(function(){$(".futurD:last").removeClass("dieEffect")},100);
}

function win(){
  if(remaining <= 0){
    $(".win").css({
      "display": "flex",
      "flex-direction" : "column",
      "justify-content" : "space-around",
    });
    $(".win").draggable({
      containment : "parent"
    });
    checkScore();
  }
}

function checkScore(){
  if(score < 200){
    $(".win").append('<div class="sous-texte">Le monde se consumme dans les flammes, mais tu nous as sauvé donc mercé.</div>');
  }
  else if (score > 200 && score < 350){
    $(".win").append("<div class='sous-texte'>Ton strabisme n'a été fatal qu'à la Creuse bien ouej.</div>");
  }
  else if (score >350 && score < 450){
    $(".win").append("<div class='sous-texte'>Score moyen donc info moyenne : pendant l'intégralité de ta vie tu produis 3,500 Tonnes de matières fécales.")
  }
  else if ( score > 450 && score < 549){
    $(".win").append("<div class='sous-texte'>Résultat encourageant, mais bavarde en classe.</div>")
  }
  else if (score == 550){
    $(".win").append("<div class='sous-texte'>Score parfait incroyable !!! Mais pas autant qu'un tacos trois viandes bien essayé</div>")
  }
}

function alienMouv(){
  $(".mouv_left").animate({
    width : "350px",
  },10000);
  $(".mouv_left").animate({
    width : "-350px",
  },10000)
  setTimeout(function(){$(".mouv_right").animate({
    width : "350px",
  },10000);
  $(".mouv_right").animate({
    width : "-350px",
  },10000)},10000)
  alienMouv();
  };
alienMouv();

function deletVoid(){
  $(".allyShoot").css({
    display : "none",
  });
  $(".gameContainer").append('<div class="futurV"></div>');
  $(".futurV:last").addClass("voidEffect");
  $(".voidEffect").css({
    "top" : alpost2 + 5,
    "left": alposl2 - 15,
  })
  score = score - 10;
  $(".scorejs").html(score);
  setTimeout(function(){$(".futurV:last").removeClass("voidEffect")},300);


}
var alposl;
var alpost;
var alposl2;
var alpost2;
var tanktop;
var tankleft;


function checkColision () {
  var boum = false;
  $('.ouaisu').each(function () {
    if (colision($('.allyShoot:last'), $(this))) {
      boum = true;
      return;
    }
  });
  return boum;
}


function colision (elt1, elt2) {
  var p_elt1 = elt1.position();
  var p_elt2 = elt2.position();
  
  if (p_elt1.left < p_elt2.left + elt2.width() &&
     p_elt1.left + elt1.width() > p_elt2.left &&
     p_elt1.top < p_elt2.top + elt2.height() &&
     elt1.height() + p_elt1.top > p_elt2.top) {
      alposl = p_elt2.left;
      alpost = p_elt2.top;
      return true;
  }
}

function checkColision2 () {
  var boum = false;
  $('.scoreboard').each(function () {
    if (colision2($('.allyShoot:last'), $(this))) {
      boum = true;
      return;
    }
  });
  return boum;
}

function colision2 (elt1, elt2) {
  var p_elt1 = elt1.position();
  var p_elt2 = elt2.position();
  
  if (p_elt1.left < p_elt2.left + elt2.width() &&
     p_elt1.left + elt1.width() > p_elt2.left &&
     p_elt1.top < p_elt2.top + elt2.height() &&
     elt1.height() + p_elt1.top > p_elt2.top) {
      alposl2 = p_elt1.left;
      alpost2 = p_elt1.top;
      return true;
  }
}

function checkColision3 () {
  var boum = false;
  $('.enemyShoot').each(function () {
    if (colision3($('.tank'), $(this))) {
      boum = true;
      return;
    }
  });
  return boum;
}


function colision3 (elt1, elt2) {
  var p_elt1 = elt1.position();
  var p_elt2 = elt2.position();
  
  if (p_elt1.left < p_elt2.left + elt2.width() &&
     p_elt1.left + elt1.width() > p_elt2.left &&
     p_elt1.top < p_elt2.top + elt2.height() &&
     elt1.height() + p_elt1.top > p_elt2.top) {
      tanktop = p_elt1.top;
      tankletft = p_elt1.left;
      return true;
  }
}


// var monsterBoard = [
//   [0,0,0,0,0,0,0,0,0,0,0],
//   [0,0,0,0,0,0,0,0,0,0,0],
//   [0,0,0,0,0,0,0,0,0,0,0],
//   [0,0,0,0,0,0,0,0,0,0,0],
//   [0,0,0,0,0,0,0,0,0,0,0],
// ];

// co = 55;



// function monsterAssign(){
//   while(co > 0){
//   if(co > 44){
//     var aaaaa = $(".l1");
//     var col = monsterBoard[0];
//     var popo = col.indexOf(0);
//     $(aaaaa[popo]).data("numbera", co);
//     console.log($(aaaaa[popo]).data('numbera'))
//     col[popo] = 1;
//   }
//   if (co <=44 && co >33){
//     var aaaaa = $(".l2");
//     var col = monsterBoard[1];
//     var popo = col.indexOf(0);
//     $(aaaaa[popo]).data("numbera", co);
//     console.log($(aaaaa[popo]).data('numbera'))
//     col[popo] = 1;
//   }
//   if (co <= 33 && co >22){
//     var aaaaa = $(".l3");
//     var col = monsterBoard[2];
//     var popo = col.indexOf(0);
//     $(aaaaa[popo]).data("numbera", co, "Type", "gros");
//     console.log($(aaaaa[popo]).data('numbera'))
//     col[popo] = 1;
//   }
//   if (co <= 22 && co >11){
//     var aaaaa = $(".l4");
//     var col = monsterBoard[3];
//     var popo = col.indexOf(0);
//     $(aaaaa[popo]).data("numbera", co);
//     console.log($(aaaaa[popo]).data());
//     col[popo] = 1;
//   }
//   if (co <= 11){
//     var aaaaa = $(".l5");
//     var col = monsterBoard[4];
//     var popo = col.indexOf(0);
//     $(aaaaa[popo]).data("numbera", co);
//     console.log($(aaaaa[popo]).data('numbera'))
//     col[popo] = 1;
//   }
//     co --;
//   }

// }
// monsterAssign();
